<?php

namespace Kozica\TestModule\Observer;

use Magento\Framework\Event\ObserverInterface;


class Aftersave implements ObserverInterface {

protected $scopeConfig;

const XML_PATH_KOZICA_EXAMPLE = 'kozica_example/general/Testaufgabe';

   public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
   {
      $this->scopeConfig = $scopeConfig;
   }

  public function getKozicaValue() {
      $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

      return $this->scopeConfig->getValue(self::XML_PATH_KOZICA_EXAMPLE, $storeScope);

      }
  public function execute(\Magento\Framework\Event\Observer $observer)
   {
       $product = $observer->getProduct();
       $old = $product->getName();

       $newname = $old.$this->getKozicaValue();
       $product->setName($newname);


   }
}

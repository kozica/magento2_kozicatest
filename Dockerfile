FROM php:7.0-apache

MAINTAINER Pawel Kozica <paulkozica@gmail.com>

RUN a2enmod rewrite

RUN rm -rf /var/www/html/* \
    && apt-get update

# Install dependencies
RUN apt-get install -y libfreetype6-dev libicu-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev libxslt1-dev sendmail-bin sendmail sudo nano cron git 

# Configure the gd library
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/

# Install required PHP extensions
RUN docker-php-ext-install dom gd intl mbstring mcrypt pdo pdo_mysql xsl zip soap bcmath

# Install the 2.4 version of xdebug that's compatible with php7
RUN pecl install -o -f xdebug-2.4.0

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

COPY ./config/auth.json /var/www/.composer/
RUN chsh -s /bin/bash www-data
RUN chown -R www-data:www-data /var/www

RUN cd /var/www/html \
    && find . -type d -exec chmod 770 {} \; \
    && find . -type f -exec chmod 660 {} \;

# Add docker command files
ADD ./bin/* /usr/local/bin/
RUN chmod +x /usr/local/bin/install-magento
RUN chmod +x /usr/local/bin/install-sampledata
RUN chmod +x /usr/local/bin/magento-command
RUN chmod +x /usr/local/bin/deploy
RUN chmod +x /usr/local/bin/run-cron


RUN echo "memory_limit=2048M" > /usr/local/etc/php/conf.d/memory-limit.ini

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /var/www/html
#VOLUME /var/www/html/var
#VOLUME /var/www/html/pub
